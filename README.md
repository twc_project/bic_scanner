
# Wayland Scanner Operation

### Background

Each Wayland \<protocol\> has an xml file.  From it, the
wayland_scanner can be used to create several files including

  - \<protocol\>-client-protocol.h and
  - \<protocol\>-protocol.c

For a Linux disto with Wayland installed, one of these files can be
found at[^1]

 - /usr/include/wayland-client-protocol.h

### Purpose

The build process for a Wayland client must use the scanner to
generate any required files other than the one mentioned above.  The
purpose of this repository is to generate these client files for the
following protocols:

  - Wayland
  - xdg-shell
  - xdg-decoration
  - xdg-agent
  - wp-desktop-cues

The first three are part of the wayland-protocols package.  The last
two are available at [Agent Protocol].

### Operation

Header files are created and then installed to an appropriate
directory determined by `${PREFIX}/`.

  - wayland-client-protocol.h
  - xdg-shell-client-protocol.h
  - xdg-decoration-client-protocol.h
  - xdg-agent-v1-client-protocol.h
  - wp-desktop-cues-v1-client-protocol.h

Two versions of these header files are "scanned".  One for standard
(external) clients and one for Built-in (internal) Clients[^2].  They
are installed to

  - `${PREFIX}/include/twc-protocols/standard/`
  - `${PREFIX}/include/twc-protocols/built-in/`

respectively.

In addition, each \<protocol\>-protocol.c is compiled into a archive
library and similarly installed.  They are identical for both standard
and Built-in Clients, so only one set is built.

  - libwayland-client.a
  - libxdg-shell-client.a
  - libxdg-decoration-client.a
  - libxdg-agent-v1-client.a
  - libwp_desktop-cues-v1-client.a

As a convenience, the following pkgconf files are also installed.

  - twc-client-protocol-libs.pc
  - twc-protocols-std.pc
  - twc-protocols-bic.pc

This provides client build systems easy access to the above files.

### Build instructions

````
    mkdir stage
    mkdir build_std    # for standard clients
    mkdir build_bic    # for built-in clients

    PREFIX=stage

    PKG_CONFIG_DIR="${PREFIX}/lib/x86_64-linux-gnu/pkgconfig"
    PKG_CONFIG_PATH="${PKG_CONFIG_DIR}:${PREFIX}/share/pkgconfig"

    meson setup --prefix ${PREFIX} -Dbuild_type=standard \
                 -Dpkg_config_path=${PKG_CONFIG_PATH} build_std

    meson setup --prefix ${PREFIX} -Dbuild_type=built-in \
                 -Dpkg_config_path=${PKG_CONFIG_PATH} build_bic

    cd build_std
    ninja -v
    meson install

    cd build_bic
    ninja -v
    meson install
````

See the [trial_howto] for instructions on how to build the complete
TWC Project.

[^1]: The headers and libraries for the Wayland Protocol are usually
      packaged with libwayland-dev.  However, the extension protocols
      usually are not.

[^2]: See the [TWC_Project] for more context.

[Agent Protocol]: https://gitlab.com/twc_project/agent_protocol
[trial_howto]:    https://gitlab.com/twc_project/trial_howto
[TWC Project]:    https://gitlab.com/twc_project
