#! /bin/sh

 #
 # The purpoose of this script is to compose two protocol filters as
 # shown:
 #
 #         wayland scanner                        built-in-scanner
 # xyz.xml ---------------> xyz-client-protocol.h ----------------> xyz-built-in-client-protocol.h
 #
 # Wayland clients include a <protocol>-client-protocol.h.  The final
 # file in the above chain is a modified version which is used by
 # built-in clients.  See ./built-in-scanner.sh
 #
 # We're only concerned with client headers, so wayland-scanner is
 # invoked using the "client-header" form.
 #
 # Example:
 #
 #   ./compose.sh /usr/bin/wayland-scanner ./built-in-scanner.sh /usr/share/wayland/wayland.xml wayland-client-protocol.h
 #

scanner_mode="client-header"

  wayland_scanner="$1"
 built_in_scanner="$2"
          xml_src="$3"
built_in_client_h="$4"  # output file name

if    [ "$#" -ne 4             ] || \
    ! [ -x "$wayland_scanner"  ] || \
    ! [ -x "$built_in_scanner" ] || \
    ! [ -e "$xml_src"          ]    \
; then
    printf "$0: Error\n"
    exit 1
fi

  # Temp file to contain the standard client-protocol.h file, i.e.,
  # the intermediate file.
standard_client_h=$(
    /bin/mktemp --suffix=.out TEMP_XXXXXXXXXX
)
trap "rm ${standard_client_h}" EXIT

  # Create a temp version of the standard client-protocol.h file.
$wayland_scanner $scanner_mode $xml_src $standard_client_h

  # Use the built-in-scanner to output a client header file for
  # built-in clients.
$built_in_scanner -i $standard_client_h -o $built_in_client_h

rm ${standard_client_h}
trap - EXIT

exit 0
