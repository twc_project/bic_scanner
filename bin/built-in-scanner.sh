#! /bin/sh

 # Usage:
 #
 #   buitl-in-scanner.sh [ -i <in_file> -o <out_file> ]
 #
 # Default  in_file:    /usr/include/wayland-client-protocol.h
 #
 # Default out_file:    /dev/stdout
 #

 #
 # Note that protocol include files are created using the
 # wayland_scanner as follows:
 #
 #   $ wayland_scanner client-header <protocol>.xml <protocol>-client-protocol.h
 #
 # This script is a filter.  It takes a <protocol>-client-protocol.h
 # file as produced by the wayland_scanner and converts it to be
 # suitable for Built-in Clients.  An explanation, based on
 # wayland-client-protocol.h, is presented next.
 #

 #
 # The standard Wayland include file for clients
 #
 #     wayland-client-protocol.h
 #
 # defines inline-functions, each of which represents a client-side
 # proxy for a server-side instantiation of the function.  Each
 # inline-fucntion has a specification consisting of a declaration
 # part and a body definition part.  For buili-in clients, the body
 # part is not needed.  However, the declaration part is.
 #
 # The purpose of this script is to convert the inline-function
 # definitions into extern function declarations.
 #
 # The Wayland client include file contains inline function
 # specifications similar to those which follow:
 #
 #     static inline void
 #     wl_display_set_user_data(struct wl_display *wl_display, void *user_data)
 #     {
 #             wl_proxy_set_user_data((struct wl_proxy *) wl_display, user_data);
 #     }
 #     
 #     static inline int
 #     wl_display_add_listener(struct wl_display *wl_display,
 #                             const struct wl_display_listener *listener, void *data)
 #     {
 #             return wl_proxy_add_listener((struct wl_proxy *) wl_display,
 #                                          (void (**)(void)) listener, data);
 #     }
 #
 #
 # The script uses awk to filter out the body definition for each
 # inline proxy and add a necessary ";" to the declaration.
 # Additionally, the declaration is converted from "static inline" to
 # "extern".  The result for the above examples should look like this.
 #
 #     extern void
 #     wl_display_set_user_data(struct wl_display *wl_display, void *user_data);
 #
 #     extern int
 #     wl_display_add_listener(struct wl_display *wl_display,
 #                             const struct wl_display_listener *listener, void *data);
 #

 #
 # In addtion to the problematic proxy function definitions, each
 # standard <protocol>-client-protocol.h file also contains a line
 #
 #   #include "wayland-client.h"
 #
 # This also causes an issue.  To understand this issue and fix it, we
 # must consider the cascading include tree.  It is presented next.
 #
 # <protocol>-client-protocol.h
 # 
 #     wayland-client.h                      // include Line A,
 #                                           // pulls in the following:
 #         wayland-client-core.h
 # 
 #             wayland-util.h                // include Line B
 # 
 #             declare wl_proxy machinery    // safe, but not needed
 #             declare wl_display_...        // needed
 # 
 #         wayland-client-protocol.h         // even for xdg-shell etc.
 # 
 #             wayland-client.h
 # 
 #             defines proxy inline functions  // problematic
 # 
 # Each client-protocol header file includes wayland-client-protocol.h
 # which pulls in additional headers and declarations as shown above.
 # The last line, reults in the proxy definition problem(*).
 #
 # The solution is to simply replace Line A above with
 # wayland-client-core.h as shown below.  This fix produces the
 # following include tree.
 #
 #  <protocol>-client-protocol.h
 # 
 #     wayland-client-core.h
 # 
 #         wayland-util.h
 # 
 #         declare wl_proxy machinery    // safe, but not needed
 #         declare wl_display_...        // needed
 # 
 # 
 # (*) One might think that the Built-in version of
 #     wayland-client-protocol.h would be included in the cascade.
 #     However, it is the system version of wayland-client.h that is
 #     included and it uses the syntax
 #
 #         #include "wayland-client-protocol.h"
 #
 #     To get the Built-in version this line would have to be
 #
 #         ${PREFIX}/include/twc/twc-protocols/built-in/wayland-client-protocol.h"
 #

USAGE="Usage: `basename $0` [ -i <in_file> -o <out_file> ]"

 In_File="/usr/include/wayland-client-protocol.h"
Out_File="/dev/stdout"

while getopts 'hi:o:' OPT ; do
    case "$OPT" in
      h)  printf "%s\n" "${USAGE}"
          exit 0
          ;;
      i)  In_File=${OPTARG}
          ;;
      o)  Out_File=${OPTARG}
          ;;
      *) >&2 printf "%s\n" ${USAGE}
          exit 1
    esac
done
shift `expr $OPTIND - 1`


 #
 # The awk script below uses more than one 'pattern { action }'
 # clause as shown below.
 #
 #     awk  pattern { action }
 #          pattern { action }
 #
 # A pattern is some form of match-expression involving a regex.
 #
 # This awk script also uses the "record range" feature.  This feature
 # can match a range of records (lines).  In this case , the pattern
 # consists of two match-expressions separated by a comma.  For
 # example,
 #
 #     awk  regex1, regex2 { action }
 #
 # where regex1 demarc's the first record in the range and regex2 the
 # last record.  The action is performed on each line in the range.
 #
 # In summary we'll use this form
 #
 #     awk  regex1, regex2 { action_a }
 #          pattern        { action_b}
 #
 # The first clause deals with a range of records containing
 # inline-function specifications.  When action_a is done processing a
 # record, it skips to the next record in the range to prevent
 # action_b from firing.
 #
 # The second clause outputs all the other lines untouched.
 #
 # Also note that for sed, the typical idiom for substituion
 #
 #    s/pattern/replacenent/
 #
 # is equivalent to
 #
 #    s%pattern%replacement%
 #
 # The former is handy when the pattern or replacenent strings contain
 # slashes.
 #
 # Example:  The followings lines
 #
 #     static inline void
 #     wl_surface_commit(struct wl_surface *wl_surface)
 #     {
 #           wl_proxy_marshal_flags((struct wl_proxy *) wl_surface,
 #                            WL_SURFACE_COMMIT, NULL, wl_proxy_get_version((struct wl_proxy *) wl_surface), 0);
 #     }
 #
 # are transformed into the single line
 #
 #     wl_surface_commit(struct wl_surface *wl_surface);
 #
 # The range action simply removes the first line (static inline) and
 # all the lines enclosed in braces {...}.
 #

cat $In_File | \
awk -v decl=0 '

    # Begin first clause

        first = 0;

          # Match on the first line of an inline specification.
          # This begins the static inline declaration part.
          # The ^ matches the beginning of a line
        first = /^static inline /,
        
          # Match on the last line of an inline specification.
          # It will be a line whose first field is a close curly brace.
        ( $1 ~  "}" ) {
        
              # While processing an inline specification, 
              # Match on the end of the declaration part.
              # It will be a line whose last field ends with a close paren.
              # We will need to add a trailing semi-colon.
            end_decl = 0;
            end_decl = ( $NF ~ "[[:print:]]*[)]$" );
        
              # The next three lines must be in this order.
            if ( end_decl == 1 ) { decl = 0; printf "%s;\n", $0 }
            if ( decl     == 1 ) {           print              }
            if ( first    == 1 ) { decl = 1; printf "extern";for(i=3;i<=NF;i++) printf " %s", $i; printf "\n" }

            next  # goto next record in range
        }
    # End first clause
#
    # Begin second clause

          # This pattern matches all lines that were not subject to
          # action_a above.
        /.*/ { print }

    # End second clause
' | \
sed -e 's%\(.*include.*wayland-client.h.*\).*%#include "wayland-client-core.h"    /* \1 */\n#include "wayland-util.h"%' \
>> $Out_File

exit 0
